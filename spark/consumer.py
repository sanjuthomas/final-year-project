from kafka import KafkaConsumer
from pyspark.ml import Pipeline,PipelineModel
from pyspark.sql import SparkSession
from pyspark.sql.types import StringType
import json
from mysql.connector import connect, Error


spark = SparkSession.builder \
    .appName("Spark Structured Streaming from Kafka") \
    .getOrCreate()

consumer = KafkaConsumer('twitterstream')
model = PipelineModel(stages=[]).load("/home/clementcj/lampstack-8.1.6-0/apache2/htdocs/final-year-project/ml_model/model")

def insert_tweet(tweet_id, username, tweet, prediction):
    HOST = "localhost"
    PORT = 3307
    USER = "sanju"
    DB = "railway"
    PASSWORD = "mariadb"

    try:
        with connect(host=HOST, user=USER, password=PASSWORD, database=DB, port=PORT) as conn:
            cursor = conn.cursor()
            query = ("INSERT INTO "
                     "tweets(tweet_id, username, tweet, prediction)"
                     "VALUES(%s, %s, %s, %s)")
            data = (tweet_id, username, tweet, prediction)
            cursor.execute(query, data)
            print("[INFO] DB insertion successfull!")
            conn.commit()
    except Error as e:
        print(e)
        print("[INFO] DB insertion unsuccessfull!")

for msg in consumer:     
    value = json.loads(msg.value.decode())
    tweet = value['data']['text']
    if (not tweet.startswith('@railwaycea')):
        continue
    tweet_id = value['data']['id']
    username = value['includes']['users'][0]['name']
    df = spark.createDataFrame([(tweet, StringType())],['text'])
    prediction = model.transform(df).collect()[0]['prediction']
    print(prediction)
    insert_tweet(tweet_id, username, tweet, prediction)
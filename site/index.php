
<html>
<head>
	<title>Railway Tweets Portal</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<style>
			tr{
				border-bottom: 3px groove;
			}
			td {
		
				padding:20px;
		
			}
			.active{color:white;}
			.line{border-right: 2px groove;min-height:98.5vh;overflow-x:hidden;}
			.left{
				width:70%;
				float:left;
			}
			.right{
				float: left;
				width:25%;
			}	
			.navbar-right{
				margin-right:40px;
			}
	</style>	
	<script>
		// update emergency tweets every 5 sec
		window.setInterval(function() {
				if (window.XMLHttpRequest) {
					// code for modern browsers
					xmlhttp = new XMLHttpRequest();
				} else {
					// code for old browsers 
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.open("GET","emergency.php",true);
				xmlhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						document.getElementById("emergency-content-table").innerHTML = this.responseText;
					}
				};
				xmlhttp.send();
			
		},5000);

		// update feedback tweets every 5 sec
		window.setInterval(function() {
				if (window.XMLHttpRequest) {
					// code for modern browsers
					xmlhttp = new XMLHttpRequest();
				} else {
					// code for old browsers 
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						document.getElementById("feedback-content-table").innerHTML = this.responseText;
					}
				};
				xmlhttp.open("GET","feedback.php",true);
				xmlhttp.send();
			
		},5000);

		// selecting tweet to reply
		$(document).ready(function bind(){
			$('body').on('click','.add',function(){
				//... event handler code ....
				document.getElementById("selection").innerHTML = "";
				var $this = $(this),
				myCol = $this.closest("td"),
				myRow = myCol.closest("tr"),
				targetArea = $("#selection");
				targetArea.append(myRow.children().not(myCol).text() + "<br />");
				document.getElementById("idvalue").value = myRow.children(".value").children(".myval").val();
			});
		});

		// responding to tweets
		function sendReply(form){
			var tweet_id = form.idvalue.value;
			var tweet_reply = form.tweet_reply.value;
			console.log("tweet_id="+tweet_id+"&tweet_reply="+tweet_reply);

			if (tweet_reply) {

				if (window.XMLHttpRequest) {
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}

				xmlhttp.onreadystatechange = function() {
					if (this.readyState == 4) {
							if (this.status == 200) {
								alert('Tweet Sent !');
								form.tweet_reply.value= "";
							}	else {
								alert('Tweet Not Sent !');

							}
					}
				};
				var query = "tweet_id="+tweet_id+"&reply="+tweet_reply;
				xmlhttp.open("GET","response.php?"+query,true);
				xmlhttp.send();
			}
		}
	</script>
</head>
	<body class="line">  
		<div class="navbar" style="width:100%; padding-bottom:10px; padding-top:10px; background-color: #574b90;" id="main">
			<ul style="font-size: 29px; margin-top: 5px; cursor: pointer;">
			
				<li class="active" style="display:inline; margin-right: 15px"; data-toggle="tab" href="#emergency">
					Emergency
				</li>

				<li style="display:inline; margin-right: 15px;" data-toggle="tab" href="#feedback">
					Feedback
				</li>

				<!-- <a style=" text-decoration: none; color:black" href="chartjs.php">
					<li class="" style="display:inline; font-color:black"  href="chartjs.html">
						Tweets Analysis
					</li>
				</a> -->
				<li style="display:inline;">
					<a href="logout.php"><button class="btn btn-primary navbar-right b2" > logout </button></a>
				</li>
			</ul>
		</div>
		<div class='tab-content left'>
			<div id="emergency" class='tab-pane fade in active'>
				<div id='emergency-content' style="width:90%;padding: 50px;margin:auto;background:white; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
					<table id='emergency-content-table'>
							
					</table>
				</div>
			</div>
			<div id="feedback" class='tab-pane fade'>
				<div id='feedback-content' style="width:90%;padding: 50px;margin:auto;background:white; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
					<table id='feedback-content-table'>

					</table>
				</div>	
			</div>	
			<div id="analysis" class='tab-pane fade'>
				<div id='analysis-content' style="width:90%;padding: 50px;margin:auto;background:white; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
					<div id="app">
						<div class="main-content">
							<section class="section">
								<div class="section-body">
									<div class="row">
									<div class="col-sm-4">
									<div class="card">
									<div class="card-header">
										<h4>Emergency Tweets Chart (Montly Basis)</h4>
									</div>
									<div class="card-body">
										<canvas id="myChart"></canvas>
									</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="card">
									<div class="card-header">
										<h4>Feedback Tweets Chart (Montly Basis)</h4>
									</div>
									<div class="card-body">
										<canvas id="myChart2"></canvas>
									</div>
									</div>
								</div>
								</div>
								<div class="row">
								<div class="col-sm-4">
									<div class="card">
									<div class="card-header">
										<h4>Emergency Tweets Categorization</h4>
									</div>
									<div class="card-body">
										<canvas id="myChart3"></canvas>
									</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="card">
									<div class="card-header">
										<h4>Feedback tweets Categorization</h4>
									</div>
									<div class="card-body">
										<canvas id="myChart4"></canvas>
									</div>
									</div>
								</div>
								</div>
							</section>
						</div>
						</div>
					</div>
					</div>
				</div>		
			</div>

			<div class="container-fluid right">
				<div id="selection" style="min-height:200px;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);font-size: 16px;padding:10px;"></div>
				
				<h3 style="color:#c6c6c6;">Type your response...</h3>
				<form>
					<input type="hidden" name="idvalue" id="idvalue" value="">
	  				<textarea id="tweet_reply" name="tweet_reply" rows="10" style="width:100%;"></textarea>
					  <!-- https://www.javascript-coder.com/javascript-form/javascript-get-all-form-objects/-->
	  				<button type="button" class="btn btn-info" style="width:100%;margin-top: 10px;" onclick="sendReply(this.form)">Reply</button>
	  			</form>
			</div>
	<script>
		var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: ["11/2019", "12/2019", "01/2020", "02/2020","03/2020"],
			datasets: [{
			label: 'Emergency',
			data: [13, 12, 20, 13,9],
			borderWidth: 2,
			backgroundColor: 'rgb(87,75,144)',
			borderColor: 'rgb(87,75,144)',
			borderWidth: 2.5,
			pointBackgroundColor: '#ffffff',
			pointRadius: 4
			}]
		},
		options: {
			legend: {
			display: false
			},
			scales: {
			yAxes: [{
				ticks: {
				beginAtZero: true,
				stepSize: 9
				}
			}],
			xAxes: [{
				ticks: {
				display: false
				},
				gridLines: {
				display: false
				}
			}]
			},
		}
		});

		var ctx = document.getElementById("myChart2").getContext('2d');
		var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: ["11/2019", "12/2019", "01/2020", "02/2020","03/2020"],
			datasets: [{
			label: 'Feedbacks',
			data: [43, 32, 27, 23, 19],
			borderWidth: 2,
			backgroundColor: 'rgb(87,75,144)',
			borderColor: 'rgb(87,75,144)',
			borderWidth: 2.5,
			pointBackgroundColor: '#ffffff',
			pointRadius: 4
			}]
		},
		options: {
			legend: {
			display: false
			},
			scales: {
			yAxes: [{
				ticks: {
				beginAtZero: true,
				stepSize: 10
				}
			}],
			xAxes: [{
				ticks: {
				display: false
				},
				gridLines: {
				display: false
				}
			}]
			},
		}
		});

		var ctx = document.getElementById("myChart3").getContext('2d');
		var myChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets: [{
			data: [
				8,
				38,
				36,
				14,
				32,
			],
			backgroundColor: [
				'#574B90',
				'#28a745',
				'#ffc107',
				'#dc3545',
				'#343a40',
			],
			label: 'Dataset 1'
			}],
			labels: [
			'Medical',
			'AC Complaint',
			'Train Component',
			'Water Complaint',
			'Others'
			],
		},
		options: {
			responsive: true,
			legend: {
			position: 'bottom',
			},
		}
		});

		var ctx = document.getElementById("myChart4").getContext('2d');
		var myChart = new Chart(ctx, {
		type: 'pie',
		data: {
			datasets: [{
			data: [
				24,
				44,
				38,
				12,
				36,
			],
			backgroundColor: [
				'#574B90',
				'#28a745',
				'#ffc107',
				'#dc3545',
				'#343a40',
			],
			label: 'Dataset 1'
			}],
			labels: [
			'Train Timing',
			'Food Feedback',
			'Cleanliness',
			'Ticket Serivces',
			'Others'
			],
		},
		options: {
			responsive: true,
			legend: {
			position: 'bottom',
			},
		}
		});
  </script>
</body>
</html>

# Final year project


## Getting started


## Installation
### setting up mariadb
1. create user
```sql
    create user 'clement'@'localhost' identified by 'mariadb';
    grant all privileges on *.* to 'clement'@'locahost';
    flush privileges;
```
## TODOs
- [X] Front-End
    - [X] Feedback Module
    - [X] Emergency Module
    - [X] Tweet Analysis Module
- [ ] Back-End
    - [X] MySql setup
    - [X] Twitter API setup
    - [X] Kafka setup
    - [ ] Spark setup
    
- [ ] ML model
- [ ] Final Integration

## External Links

- [Weekly Report](https://docs.google.com/document/d/1AaKNwvyEvjRJwtG8owvrrbV5vAEpAxV-itQ5zrmOg44/edit)
- [Whiteboard](https://excalidraw.com/#room=fec92e63f4ae2b5e7b2e,uzGEefaKdCKsmZaMlrY-1g)
- [Drawings](https://app.diagrams.net/#G1IVOqGZuP4he2ryk1rL8kjQ1DGoUs2k6y)
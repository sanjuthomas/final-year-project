---
marp: true
theme: 
size: 
paginate: true
class: 
    - lead
header: '**College Of Enginneering, Adoor**'
footer: '![w:70 h:70](https://avatars.githubusercontent.com/u/34028707?s=280&v=4)'
---

# Real-Time Railway Complaint and Feedback System Using  ML

Guide: **Asst Prof Rahila N A**, Dept of CSE

###### Team Members
- Clement C Johnson(ADR18CS015)
- Lekshmi Priya M(ADR18CS026)
- Nandana Suresh(ADR18CS029)
- Sanju Thomas(ADR18CS036)

---
# Contents

1. ML model
   1. Data Preprocessing
   2. Spliting the dataset
   3. Model Training
   4. Evaluation
2. Web UI
   1. Emergency Tweets Fetcher
   2. Feedback Tweets Fetcher
   3. Response Module

---
# Contents
3. Backend
   1. Database Schema
   2. Twitter API setup
   3. Kafka Producer and Server Setup
   4. Apache Spark Programming

---
# Introduction

   * Social Media is a medium to share, to inform and to reach the ideas.
   * “Twitter” - major tool to assist the railway passengers.
   * Travelers can easily tweet their situations and get its feedback.
   

---
# Existing System

   * The Indian Railways currently receives close to 5,000 Tweets a day on @RailwaySeva.
   * Complaint of sexual harassment or threat to personal safety feature on top.
   * Second come complaints against the Railway management.
   * Last come complaints and requests that are not of immediate importance. Existing System
   * The Tweets are monitored and responded .
   * The handling of complaints has been outsourced to a trained team that works in shifts.

---
# Proposed System

   * The project reduces the work complexity of scanning through thousands of
useless data to find particular information but from here we can directly find the
relevant tweets that needs attention.
   * To solve the problem, we will be using a Machine Learning (Naive Bayes) model.
   * The proposed system will divide the tweets in two category that is emergency
and feedback.
   * The manual work done by the employees will be reduced and the tweets will be
replied.

---
# Level 0 DFD

![](https://i.imgur.com/1MtcinK.png)

---
# Level 1 DFD

![](https://i.imgur.com/DVTV4mL.png)

---
# Level 2 DFD
![](https://i.imgur.com/0sXfl7l.png)

---
# ER Diagram

![](ER.png)

---
# Use-case Diagram

![h:500](use-case.png)

---

# Sequence Diagram

![](sequence.png)

---
# Data Preprocessing
![h:500](https://i.imgur.com/UkrESEB.png)

---
# Pipeline
![](https://i.imgur.com/LBR343G.png)

---
# Data Splitting
![](https://i.imgur.com/IVCZBUR.png)

---
# Model Training 
![](https://i.imgur.com/0fLIzvX.png)

---
 # Evaluation

![w: 500](https://i.imgur.com/6ZJpzOd.png)
![w :500](https://i.imgur.com/UZuwfIq.png)

---


![](https://i.imgur.com/Vjsb6E1.png)
![](https://i.imgur.com/LCaHRD7.png)

---
# Model Saving 
![](https://i.imgur.com/DPc9u4f.png)

---
# Web UI 

![](https://i.imgur.com/uezhKog.png)

---
# Emergency Tweet Fetcher Module(JS)
![](https://i.imgur.com/fSdlZU5.png)

---
# Emergency Tweet API Module(PHP)

![h:500](https://i.imgur.com/wsG06JQ.png)

---

# Feedback Tweet Fetcher Module(JS) 

![](https://i.imgur.com/Alftumh.png)

---

# Feedback Tweet API Module
![w:800](https://i.imgur.com/HatsFIC.png)

---

#  Response API Module(PHP)

![h:500](https://i.imgur.com/9lFuMQv.png)

---
# Response API Module(Continued)

![](https://i.imgur.com/D9000wL.png)

---

# Database Schema

![](https://i.imgur.com/R0gWP5e.png)

---

# Kafka Producer and Server Setup

![h:500](https://i.imgur.com/7h0PXzp.png)

---
# Kafka Producer and Server Setup(Continued)

![](https://i.imgur.com/aApsN3f.png)

---
# Returned JSON Data from twitter API
![h:500](https://i.imgur.com/dj40t7f.png)

---

# Apache Spark Programming 
![](https://i.imgur.com/5ImS3o8.png)

---
# Apache Spark Programming(Continued) 
![](https://i.imgur.com/xjkeMt5.png)

---
# Apache Spark Programming(Continued)
![](https://i.imgur.com/0U1FZ18.png)

---
# Gantt Chart 
![](https://i.imgur.com/J8LrrtH.png)

---

